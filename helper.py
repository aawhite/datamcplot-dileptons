import numpy as np
try:
    from matplotlib.ticker import ScalarFormatter
    import matplotlib.ticker as ticker
except: pass
import matplotlib.pyplot as plt
import matplotlib as mpl
# mpl.use("pgf")
if mpl.__version__.split(".")[0]!="0":
    pgf_with_rc_fonts = {
        "font.family": "sans-serif",
        "font.size" : "15",
        "font.sans-serif": ["Helvetica"],
        "pgf.texsystem":"pdflatex",
        "pgf.preamble":[
                        r"\usepackage{amsmath}",\
                        r"\usepackage[english]{babel}",\
                        r"\usepackage{arev}",\
                       ]
    }
    mpl.rcParams.update(pgf_with_rc_fonts)
    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']
    # plt.rcParams["patch.force_edgecolor"] = True
    plt.clf(); plt.cla()
    fig = plt.figure(1)

def ticksInside(removeXLabel=False):
    """ Make atlas style ticks """
    ax=plt.gca()
    ax.tick_params(labeltop=False, labelright=False)
    plt.xlabel(ax.get_xlabel(), horizontalalignment='right', x=1.0)
    plt.ylabel(ax.get_ylabel(), horizontalalignment='right', y=1.0)
    ax.tick_params(axis='y',direction="in",left=1,right=1,which='both')
    ax.tick_params(axis='x',direction="in",labelbottom=not removeXLabel,bottom=1, top=1,which='both')

def atlasInternal(position="nw",status="Internal",size=12,lumi=None,subnote=""):
    ax=plt.gca()
    # decide the positioning
    if position=="se":
        textx=0.95; texty=0.05; verticalalignment="bottom"; horizontalalignment="right"
    if position=="nw":
        textx=0.05; texty=0.95; verticalalignment="top"; horizontalalignment="left"
    if position=="ne":
        textx=0.95; texty=0.95; verticalalignment="top"; horizontalalignment="right"
    if position=="n":
        textx=0.5; texty=0.95; verticalalignment="top"; horizontalalignment="center"
    # add label to plot
    if lumi==None:
        lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
                 r"$\textstyle\sqrt{s}=13 \text{ TeV }$",
                ]
    else:
        lines = [r"\noindent \textbf{{\emph{{ATLAS}}}} {0}".format(status),
                 r"$\textstyle\sqrt{s}=13 \text{ TeV } "+str(lumi)+r"\text{ fb}^{\text{-1}}$",
                ]
    if subnote: lines.append(subnote)
    labelString = "\n".join(lines)
    # labelString="l=36.1 fb-1"
    plt.text(textx,texty, labelString,transform=ax.transAxes,va=verticalalignment,ha=horizontalalignment, family="sans-serif",size=size)
    # # plt.tight_layout(.5)
    # # set axis labels
    ticksInside()

