import sys,os
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
from ROOT import *
from helper import *
from math import *
from scipy.stats import chi2



class histInt:
    def __init__(self,th1):
        self.th1 = th1
        self.name = th1.GetName()
        self.nBins = th1.GetNbinsX()
        self.lo = th1.GetBinLowEdge(1)
        self.hi = th1.GetBinLowEdge(self.nBins+1)
        self.width = th1.GetBinLowEdge(2)-th1.GetBinLowEdge(1)
        self.y = np.array([th1.Integral(i,self.nBins) for i in range(1,self.nBins+1)])
        # self.y = np.array([th1.GetBinContent(i) for i in range(1,self.nBins+1)])
        self.x = np.array([th1.GetBinCenter(i) for i in range(1,self.nBins+1)])


    def __str__(self):
        return "histInt {0} nBins={1} [{2},{3}]".format(self.name,self.nBins,self.lo,self.hi)

def loadHisto(path,name,rebin,scale=1):
    print "Load",path, name
    f = TFile.Open(path)
    h = f.Get(name)
    print "Scaling",scale
    h.Scale(scale)
    h.Rebin(int(rebin))
    return histInt(h)


def poisson_interval(v):
    if v==0: return 0,0
    # from Ntuple reader code:
    y1 = v + 1.0
    d1 = 1.0 - 1.0/(9.0*y1) + 1.0/(3*sqrt(y1))
    hi = y1*d1*d1*d1 - v
    y2 = v
    d2 = 1.0 - 1.0/(9.0*y2) - 1.0/(3.0*sqrt(y2))
    lo = v - y2*d2*d2*d2
    return [lo,hi]



def plot(interference,path="plots/example.png"):
    """ Make plot """
    plt.clf(); plt.cla()
    plt.figure(figsize=(6,6))


    # plot MC background
    dyColor      = "#496D89"
    ttbarColor   = "#55AA55"
    dibosonColor = "#D4A76A"
    fakesColor   = "#FFDBAA"

    sig1Color = "#FFAAAA"
    sig2Color = "#D46A6A"
    sig3Color = "#AA3939"
    sig4Color = "#801515"
    sig5Color = "#550000"

    width = dy.width
    bottom = np.zeros(len(dy.x))

    bkgs = []
    bkgs.append(plt.bar(fakes.x,fakes.y,width=width,bottom=bottom,     color=fakesColor,   label=r"Multijet"))
    bottom+=fakes.y
    bkgs.append(plt.bar(diboson.x,diboson.y,width=width,bottom=bottom, color=dibosonColor, label=r"Diboson"))
    bottom+=diboson.y
    bkgs.append(plt.bar(ttbar.x,ttbar.y,width=width,bottom=bottom,     color=ttbarColor,   label=r"$t\bar{t}$"))
    bottom+=ttbar.y
    bkgs.append(plt.bar(dy.x,dy.y,width=width,bottom=bottom,           color=dyColor,      label=r"Z/$\gamma^*$"))
    bottom+=dy.y

    # check binning cut
    if dy.x[0]!=sig1.x[0]:
        raise BaseException("Sig and bkg miss-match")
    if (dy.x[0:len(sig1.x)] != sig1.x).all():
        raise BaseException("Sig and bkg miss-match")
    sigBot = bottom[0:len(sig1.x)]
    #
    plt.step(sig1.x,sig1.y+sigBot,color=sig1Color,label=r"LL-{0} ($\Lambda=12$ TeV)".format(interference))
    plt.step(sig2.x,sig2.y+sigBot,color=sig2Color,label=r"LL-{0} ($\Lambda=20$ TeV)".format(interference))
    plt.step(sig3.x,sig3.y+sigBot,color=sig3Color,label=r"LL-{0} ($\Lambda=30$ TeV)".format(interference))
    plt.step(sig4.x,sig4.y+sigBot,color=sig4Color,label=r"LL-{0} ($\Lambda=40$ TeV)".format(interference))
    sigs = []
    sigs.append(plt.plot([],[],linewidth=10,color=sig1Color,label=r"$\Lambda=12$ TeV"))
    sigs.append(plt.plot([],[],linewidth=10,color=sig2Color,label=r"$\Lambda=20$ TeV"))
    sigs.append(plt.plot([],[],linewidth=10,color=sig3Color,label=r"$\Lambda=30$ TeV"))
    sigs.append(plt.plot([],[],linewidth=10,color=sig4Color,label=r"$\Lambda=40$ TeV"))


    loErr = np.array([poisson_interval(i)[0] for i in data.y])
    hiErr = np.array([poisson_interval(i)[1] for i in data.y])
    loErr = loErr[data.y>0]
    hiErr = hiErr[data.y>0]
    data.x= data.x[data.y>0]
    data.y= data.y[data.y>0]
    bkgs.append(plt.errorbar(data.x,data.y,yerr=[loErr,hiErr],markersize=3,marker="o",linewidth=1,linestyle="",color="k",label="Data"))


    plt.yscale("log")
    plt.xscale("log")
    plt.ylim(top=plt.ylim()[1]*100)
    plt.ylim(bottom=1e-3)
    plt.ylabel("Events above mass")
    plt.xlabel(r"$M_{ee}$ [GeV]")
    if interference=="const":
        subnote = r"$ee$ Selection\\Construcitve Signals"
    else:
        subnote = r"$ee$ Selection\\Destructive Signals"
    atlasInternal(lumi=139,subnote=subnote)
    l1 = plt.legend(bkgs,[o.get_label() for o in bkgs],loc=9,frameon=0,fontsize=10)
    l2 = plt.legend([o[0] for o in sigs],[o[0].get_label() for o in sigs],loc=1,frameon=0,fontsize=9)
    plt.gca().add_artist(l1)
    plt.xlim(min(data.x),6000)

    ax = plt.gca()
    ax.tick_params(axis = 'both', which = 'major', labelsize = 15)
    ax.xaxis.set_major_locator(ticker.LogLocator(base=10.0, numticks=15))
    ax.xaxis.set_major_formatter(ticker.ScalarFormatter())
    ax.set_xticks([200,500,1000,6000])

    # blind
    plt.xlim(right=2e3)

    plt.savefig(path)
    plt.savefig(path.replace(".png",".pdf"))


os.popen("rm plots/*")
for interference in ["const","dest"]:

    iPathMc = "../SharedInputsProj/rel21_templates/merged_ee.root"
    iPathData = "../SharedInputsProj/rel21_dataTemplates/mInv_spectrum_combined_ll_rel21_fullRun2.root"
    iPathSig = "../SharedInputsProj/rel21_ci/templates_r21_ee_new/CI_Template_LL.root"
    nameFakes  = "ee_201516fakes"
    nameTtbar  = "ee_diboson_smoothTTbar"
    nameDy     = "ee_diboson_smoothDY"
    nameDiboson= "ee_dibosonNoOutliers"
    nameData   = "mInv_spectrum_201518_ee_1GeV"
    nameSig1   = "diff_CI_LL_{0}_12_TeV".format(interference)
    nameSig2   = "diff_CI_LL_{0}_20_TeV".format(interference)
    nameSig3   = "diff_CI_LL_{0}_30_TeV".format(interference)
    nameSig4   = "diff_CI_LL_{0}_40_TeV".format(interference)
    rebin = 100

    fakes   = loadHisto(iPathMc,nameFakes,rebin=rebin)
    ttbar   = loadHisto(iPathMc,nameTtbar,rebin=rebin)
    dy      = loadHisto(iPathMc,nameDy,rebin=rebin)
    diboson = loadHisto(iPathMc,nameDiboson,rebin=rebin)
    data    = loadHisto(iPathData,nameData,rebin=10)
    sig1    = loadHisto(iPathSig,nameSig1,rebin=rebin/10,scale=139/80.5)
    sig2    = loadHisto(iPathSig,nameSig2,rebin=rebin/10,scale=139/80.5)
    sig3    = loadHisto(iPathSig,nameSig3,rebin=rebin/10,scale=139/80.5)
    sig4    = loadHisto(iPathSig,nameSig4,rebin=rebin/10,scale=139/80.5)

    plot(interference,path="plots/eeMc-{0}.png".format(interference))
